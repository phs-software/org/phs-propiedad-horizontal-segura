export const personsMock = [
    {
        firstName: "Name0",
        lastName: "Lastname0",
        email: "email0@host.tld",
    },
    {
        firstName: "Name1",
        lastName: "Lastname1",
        email: "email1@host.tld",
    },
    {
        firstName: "Name2",
        lastName: "Lastname2",
        email: "email2@host.tld",
    },
];

export const personMock = personsMock[0];

export const personsAPIResponseMock = personsMock;
