<!---------------------------------------------------------------------------->
<!-- Copy the base requirement here -->
Base Requirement: REQ-{N} {Description}

<!--
Define the user story:
-->

> In order to <receive benefit> as a <role>, I can <goal/desire>

<!---------------------------------------------------------------------------->
# Tasks
<!--
Which tasks needs to be done in order to finish this requirement?

The first task that should be performed is the refinement in order to understand
the task scope.

Tasks should be defined as:

* [ ] #{N} {Description}

where:

* {N} Is the number/id of created issue.
* {Description} is the actual description/title defined in the issue.
-->

* [ ] ~"Type::Refinement" Task: #nnn
* [ ] #nnn Task 1
* [ ] #nnn Task 2
* [ ] #nnn Task 3

<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Type::User Requirement" ~"Triaging:Stage::New"

