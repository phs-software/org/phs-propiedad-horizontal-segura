<!---------------------------------------------------------------------------->
**Sprint**: Sprint-N %Sprint-N
<!-- Describe the main goal of this sprint -->



<!---------------------------------------------------------------------------->
# Commitments

<!--........................................................................-->
## Developer A

* #N Issue 1 (`estimated time`)
* #N Issue 2 (`estimated time`)

<!--........................................................................-->
## Developer B

* #N Issue 3 (`estimated time`)
* #N Issue 4 (`estimated time`)

<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Type::Sprint Planning"

