<!---------------------------------------------------------------------------->
<!-- Context -->
Base Issue: #{N} {Issue description}
Branch: `{Branch goes here}`



<!---------------------------------------------------------------------------->
# Users

* Role: {role}
* Credentials: U: {user}, P: {password}

<!---------------------------------------------------------------------------->
# Testing cases (user cases)

* Case A.
* Case B.
* Case C.

<!---------------------------------------------------------------------------->
# Notes
<!--
Is there somthing testers should have into account?
-->

<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Type::Testing Session"

