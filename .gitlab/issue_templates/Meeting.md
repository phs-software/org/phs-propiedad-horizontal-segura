<!---------------------------------------------------------------------------->
<!-- Describe the meeting goal, why is it required? -->



<!---------------------------------------------------------------------------->
# Assistants

* **Organizer**: @organizer
* **Participants**: @phs-software/developers 

<!---------------------------------------------------------------------------->
# Agenda

* **Point A**: Brief description.
* **Point B**: Brief description.
* **Point C**: Brief description.

<!---------------------------------------------------------------------------->
# Notes taken during the meeting



<!---------------------------------------------------------------------------->
# Compromises

* @ personA: Compromise A.
* @ personB: Compromise B.

<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Type::Meeting" ~"Triaging:Stage::New"

