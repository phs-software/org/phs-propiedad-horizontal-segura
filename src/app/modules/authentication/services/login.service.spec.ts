import { TestBed } from '@angular/core/testing';

import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData, asyncError } from 'tests/helpers';

import { LoginService } from './login.service';

describe('LoginService', () => {
  let service: LoginService;
  let httpClientSpy: {
    get: jasmine.Spy,
    patch: jasmine.Spy,
    post: jasmine.Spy
  };
  let authenticationSpy: {
    sessionToken: jasmine.Spy,
    saveSession: jasmine.Spy,
    cleanSession: jasmine.Spy,
  };
  const APIHeaders = API_HEADERS;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch', 'post']);
    authenticationSpy = jasmine.createSpyObj('AuthenticationService',
                                             [
                                                'sessionToken',
                                                'saveSession',
                                                'cleanSession',
                                              ]);
    service = new LoginService(httpClientSpy as any, authenticationSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it ('should login an user', async () => {
    let returnedObject;

    const username = 'cooldude6@cooldomain.com';
    const password = 'S0meÑP@ssword';

    const sessionMock = {
      username,
      phone: '415-392-0202',
      objectId: 'g7y9tkhB7O',
      sessionToken: 'r:pnktnjyb996sj4p156gjtp4im',
    };

    const params = {
      username,
      password,
    };

    const endpoint = API_BASE + '/login';

    httpClientSpy.get.and.returnValue(asyncData(sessionMock));

    returnedObject = await service.logIn(username, password);

    expect(returnedObject).toEqual(sessionMock);
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.get.calls.first().args[1]).toEqual({headers: APIHeaders, params});
  });

  it('should logout an user', async () => {
    authenticationSpy.sessionToken.and.returnValue('r:pnktnjyb996sj4p156gjtp4im');

    const sessionToken = authenticationSpy.sessionToken;

    const sessionTokenHeader = {
      'X-Parse-Session-Token' : sessionToken,
    };

    const headers = Object.assign(sessionTokenHeader, API_HEADERS);

    const options = {
      headers
    };

    const endpoint = API_BASE + '/logout';

    await service.logOut();

    expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.post.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.post.calls.first().args[2]).toEqual(options);
  });
});
