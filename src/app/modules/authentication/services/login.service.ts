import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { API_BASE, API_HEADERS } from '@env/environment';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(protected httpClient: HttpClient, private authentication: AuthenticationService) { }

  /**
   * Allow an user to login in the app
   */
  public logIn(username: string, password: string): Promise<any> {
    const APIEndPoint = API_BASE + '/login';
    const params = {
      username,
      password
    };
    const options = {
      headers: API_HEADERS,
      params,
    };

    const session = this.httpClient.get(APIEndPoint, options)
      .toPromise();

    session.then(sessionInfo => this.authentication.saveSession(JSON.stringify(sessionInfo)));

    return session;
  }

  /**
   * Allow an user to log out
   */
  public logOut() {
    const sessionToken = this.authentication.sessionToken;
    const APIEndPoint = API_BASE + '/logout';
    const sessionTokenHeader = {
      'X-Parse-Session-Token' : sessionToken,
    };
    const headers = Object.assign(sessionTokenHeader, API_HEADERS);
    const options = {
      headers
    };

    this.httpClient.post(APIEndPoint, null, options);

    this.authentication.cleanSession();
  }
}
