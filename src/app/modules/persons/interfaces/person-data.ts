export interface PersonData {
    id: any;
    firstName: string;
    lastName: string;
    email: string;
}
