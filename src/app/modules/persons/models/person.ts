import { Object } from 'parse';

import { PersonData } from '../interfaces/person-data';

export class ParsePerson extends Object {}
Object.registerSubclass('Person', ParsePerson);

export class Person implements PersonData {
    public id: any;
    public firstName: string;
    public lastName: string;
    public email: string;
    private _residency: any;

    public get isResident() {
        // TODO: Implement me.
        return false;
    }

    public get residency() {
        // TODO: Implement me.
        return null;
    }

    public constructor(entity: ParsePerson|PersonData = null) {
        if (entity instanceof ParsePerson) {
            this.updateData(entity);
        } else if (entity) {
            this.id = entity.id;
            this.firstName = entity.firstName;
            this.lastName = entity.lastName;
            this.email = entity.email;
        }
    }

    public updateData(entity: ParsePerson) {
        this.firstName = entity.get('firstName');
        this.lastName = entity.get('lastName');
        this.email = entity.get('email');
        this._residency = entity.get('residency');
        if (!entity.isNew()) {
            this.id = entity.id;
        }
    }
}
