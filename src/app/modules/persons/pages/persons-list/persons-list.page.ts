import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { PersonsFormPage } from '../persons-form/persons-form.page';
import { Person } from '../../models/person';
import { PersonsService } from '../../services/persons.service';
import { PersonData } from '../../interfaces/person-data';

@Component({
  selector: 'app-persons-list-page',
  templateUrl: './persons-list.page.html',
  styleUrls: ['./persons-list.page.scss'],
})
export class PersonsListPage implements OnInit {
  public residents: Person[];

  constructor(public modalController: ModalController,
              private personsService: PersonsService
              ) { }

  ngOnInit() {
    this.loadResidents();
  }

  public async loadResidents() {
    this.personsService.loadResidents();
    this.residents = this.personsService.residents;
    console.log('residents', this.residents);
  }

  public goToCreateResident() {
    this.goToResidentForm();
  }

  public onEditResident(residentData: PersonData) {
    this.goToResidentForm(residentData);
  }

  public onRemoveResident(personData: PersonData) {
    const resident = this.personsService.createNew(personData);
    this.personsService.remove(resident);
  }


  public async goToResidentForm(personData: PersonData = null) {
    const modal = await this.modalController.create({
       component: PersonsFormPage,
       componentProps: {personData},
      });

    await modal.present();
  }
}

