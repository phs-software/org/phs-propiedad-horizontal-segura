import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonsListPage } from './persons-list.page';

const routes: Routes = [
  {
    path: '',
    component: PersonsListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonsListPageRoutingModule {}
