import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Person } from '../../models/person';
import { PersonData } from '../../interfaces/person-data';

@Component({
  selector: 'app-persons-list',
  templateUrl: './persons-list.component.html',
  styleUrls: ['./persons-list.component.scss'],
})
export class PersonsListComponent implements OnInit {
  @Input()
  public persons: Person[];
  @Output()
  public editResident: EventEmitter<PersonData> = new EventEmitter();
  @Output()
  public removeResident: EventEmitter<PersonData> = new EventEmitter();


  public constructor() { }

  public ngOnInit() { }

  public edit(person: PersonData) {
    this.editResident.emit(person);
  }

  public remove(person: PersonData) {
    this.removeResident.emit(person);
  }

}
