import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { PersonData } from '../../interfaces/person-data';

@Component({
  selector: 'app-persons-form',
  templateUrl: './persons-form.component.html',
  styleUrls: ['./persons-form.component.scss'],
})
export class PersonsFormComponent implements OnInit {
  @Input()
  public personData: PersonData;

  @Output()
  public submitForm: EventEmitter<{email: string, firstName: string, lastName: string}> = new EventEmitter();
  @Output()
  public cancelForm: EventEmitter<void> = new EventEmitter();

  private personForm: FormGroup;

  public constructor(
    private formBuilder: FormBuilder
  ) {
    this.personForm = this.formBuilder.group({
        email: ['', [Validators.email, Validators.required]],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
    });
  }

  public ngOnInit() {
    if (this.personData) {
      this.personForm.setValue({
        email: this.personData.email,
        firstName: this.personData.firstName,
        lastName: this.personData.lastName,
      });
    }
  }

  public onSubmit(): void {
    const value = this.personForm.value;

    if (!this.personForm.valid) {
      // TODO: Define an error handling method.
      return;
    }

    if (this.personData) {
      value.id = this.personData.id;
    }

    this.submitForm.emit(value);
  }

  public onCancelForm(): void {
    this.cancelForm.emit();
  }
}
