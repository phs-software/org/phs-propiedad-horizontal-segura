import { Component, OnInit, Input } from '@angular/core';

import { User } from '../../models/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  @Input()
  public user: User;
  public userName: string;

  constructor() { }

  ngOnInit() {
    this.userName = this.user.username;
  }

}
