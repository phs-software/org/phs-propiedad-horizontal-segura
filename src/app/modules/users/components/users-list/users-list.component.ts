import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { UserComponent } from '../user/user.component';
import { User } from '../../models/user';
import { UserData } from '../../interfaces/user-data';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements OnInit {
  @Input()
  public users: User[];

  @Output()
  public editUser: EventEmitter<UserData> = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  public edit(user: UserData) {
    this.editUser.emit(user);
  }

}
