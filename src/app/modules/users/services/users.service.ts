import { Injectable } from '@angular/core';
import * as Parse from 'parse';

import { User } from '../models/user';
import { UserData } from '../interfaces/user-data';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  public users: User[];

  constructor() {}

  /**
   * Loads the user data and returns the model.
   */
  public async getUser(id: any): Promise<User> {
    const parseUser = await this.loadParseUser(id);
    return new User(parseUser);
  }

  /**
   * creates a new user front-end
   */
  public createNew(user: UserData): User {
    return new User(user);
  }

  /**
   * save user in database (list in this case)
   */
  public async save(user: User): Promise<User> {
    let parseUser: Parse.User;

    if (user.id) {
      parseUser = await this.loadParseUser(user.id);
    } else {
      parseUser = new Parse.User();
    }

    parseUser.set('username', user.username);
    parseUser.set('email', user.username);
    if (!parseUser.isNew) {
      parseUser.set('password', user.password);
    }

    await parseUser.save();
    user.updateData(parseUser);

    return user;
  }

  /**
   * Loads all the users from DB.
   */
  public loadUsers() {
    if (!this.users) {
      this.users = [];
    }
  }

  /**
   * Loads the user from the DB using parse.
   */
  private async loadParseUser(id): Promise<Parse.User> {
    const query = new Parse.Query(Parse.User);
    const parseUser = await query.get(id);

    return parseUser;
  }
}
