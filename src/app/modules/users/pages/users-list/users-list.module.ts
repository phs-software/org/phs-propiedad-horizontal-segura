import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsersListPageRoutingModule } from './users-list-routing.module';

import { UsersListPage } from './users-list.page';
import { UsersListComponent } from '../../components/users-list/users-list.component';
import { UserComponent } from '../../components/user/user.component';
import { UsersFormComponent } from '../../components/users-form/users-form.component';
import { UsersFormPage } from '../users-form/users-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    UsersListPageRoutingModule
  ],
  declarations: [
    UsersListPage,
    UsersListComponent,
    UserComponent,
    UsersFormComponent,
    UsersFormPage
  ],
  entryComponents: [
    UsersFormPage
  ]
})
export class UsersListPageModule {}
