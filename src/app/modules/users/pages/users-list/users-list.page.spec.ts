import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsersListPage } from './users-list.page';
import { UsersListComponent } from '../../components/users-list/users-list.component';
import { UserComponent } from '../../components/user/user.component';

describe('UsersListPage', () => {
  let component: UsersListPage;
  let fixture: ComponentFixture<UsersListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UsersListPage,
        UsersListComponent,
        UserComponent,
       ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UsersListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
