import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsersFormPage } from './users-form.page';
import { UsersFormComponent } from '../../components/users-form/users-form.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('UsersFormPage', () => {
  let component: UsersFormPage;
  let fixture: ComponentFixture<UsersFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UsersFormPage,
        UsersFormComponent,
      ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(UsersFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
