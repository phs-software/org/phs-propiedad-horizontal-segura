import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { UsersService } from '../../services/users.service';
import { User } from '../../models/user';
import { UserData } from '../../interfaces/user-data';

@Component({
  selector: 'app-users-form-page',
  templateUrl: './users-form.page.html',
  styleUrls: ['./users-form.page.scss'],
})
export class UsersFormPage implements OnInit {
  @Input()
  public user: UserData;
  constructor(
    public modalController: ModalController,
    public usersService: UsersService
  ) { }

  ngOnInit() {}

  public onSubmit(userForm: UserData) {
    this.save(userForm);
  }

  public async save(userForm: UserData) {
    const user = this.usersService.createNew(userForm);
    await this.usersService.save(user);

    this.onAfterSaveUser();
  }

  public onAfterSaveUser() {
    this.goBack();
  }

  public goBack() {
    this.modalController.dismiss();
  }
}
