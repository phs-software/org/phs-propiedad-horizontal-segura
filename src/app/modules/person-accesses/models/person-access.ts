import { PersonAccessData } from '../interfaces/person-access-data';
import { PersonData } from '@modules/persons/interfaces/person-data';
import { PropertyData } from '@modules/properties/interfaces/property-data';
import { FrontEndModel } from '@core/models/front-end-model';

export class PersonAccess extends FrontEndModel implements PersonAccessData {
    public person: PersonData;
    public property: PropertyData;

    constructor(entity: PersonAccessData) {
        super(entity);
        this.person = entity.person;
        this.property = entity.property;
    }
}
