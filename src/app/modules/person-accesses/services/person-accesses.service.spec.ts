import { PersonAccessesService } from './person-accesses.service';
import { API_BASE } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { personAccessMock, personAccessesMock } from 'tests/mocks/modules/person-accesses';

import { NotAuthorizedError } from '@errors';

describe('PersonAccessesService', () => {
    let httpClientSpy: { post: jasmine.Spy, patch: jasmine.Spy, get: jasmine.Spy };
    let service: PersonAccessesService;

    const className = 'PersonAccess';
    const APIEndpoint = API_BASE + '/classes/' + className;

    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch', 'post']);
        service = new PersonAccessesService(httpClientSpy as any);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should create a registry through PersonAccess endpoint', async () => {
        const mock = personAccessMock as any;
        const endpoint = APIEndpoint;

        httpClientSpy.post.and.returnValue(asyncData(mock));

        await service.save(mock);

        expect(httpClientSpy.patch).toHaveBeenCalledTimes(0);
        expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.post.calls.first().args[0]).toEqual(endpoint);
        expect(httpClientSpy.post.calls.first().args[1]).toEqual(mock);
    });

    it('should NOT be able to update a registry', async () => {
        const mock = Object.assign({
            id: '121212121212121212121212',
            exists: true,
        }, personAccessMock) as any;

        await expectAsync(service.save(mock)).toBeRejectedWithError(NotAuthorizedError);

        expect(httpClientSpy.patch).toHaveBeenCalledTimes(0);
        expect(httpClientSpy.post).toHaveBeenCalledTimes(0);
    });

    it('should be able to access to a specific registry', async () => {
        const id = '121212121212121212121212';
        const endpoint = APIEndpoint + '/' + id;
        const mock = Object.assign({id}, personAccessMock) as any;

        httpClientSpy.get.and.returnValue(asyncData(mock));

        const returnedObject = await service.getOne(id);

        expect(returnedObject).toEqual(mock);
        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
    });

    it('should be able to access to all registries', async () => {
        const endpoint = APIEndpoint;

        httpClientSpy.get.and.returnValue(asyncData(personAccessesMock));

        const returnedObjects = await service.getAll();

        expect(returnedObjects).toEqual(personAccessesMock);
        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
    });
});
