import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PropertiesListPage } from './properties-list.page';
import { PropertiesListComponent } from '../../components/properties-list/properties-list.component';
import { PropertyComponent } from '../../components/property/property.component';

describe('PropertiesListPage', () => {
  let component: PropertiesListPage;
  let fixture: ComponentFixture<PropertiesListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PropertiesListPage,
        PropertiesListComponent,
        PropertyComponent,
       ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PropertiesListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
