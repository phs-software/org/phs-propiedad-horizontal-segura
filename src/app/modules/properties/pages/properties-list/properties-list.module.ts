import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PropertiesListPageRoutingModule } from './properties-list-routing.module';

import { PropertiesListPage } from './properties-list.page';
import { PropertiesListComponent } from '../../components/properties-list/properties-list.component';
import { PropertyComponent } from '../../components/property/property.component';
import { PropertyFormComponent } from '../../components/property-form/property-form.component';
import { PropertiesFormPage } from '../properties-form/properties-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PropertiesListPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    PropertiesListPage,
    PropertyComponent,
    PropertiesListComponent,
    PropertiesFormPage,
    PropertyFormComponent
  ],
  entryComponents: [
    PropertiesFormPage
  ]
})
export class PropertiesListPageModule {}
