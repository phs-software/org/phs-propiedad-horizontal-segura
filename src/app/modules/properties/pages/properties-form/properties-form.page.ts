import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { PropertiesService } from '../../services/properties.service';
import { Property } from '../../models/property';
import { PropertyData } from '../../interfaces/property-data';

@Component({
  selector: 'app-properties-form-page',
  templateUrl: './properties-form.page.html',
  styleUrls: ['./properties-form.page.scss'],
})
export class PropertiesFormPage implements OnInit {
  @Input()
  public property: PropertyData;

  public constructor(
    public propertyService: PropertiesService,
    public modalController: ModalController
  ) {}

  public ngOnInit() { }

  public onSubmit(propertyForm: PropertyData) {
    this.save(propertyForm);
  }

  public async save(propertyForm: PropertyData) {
      const property = this.propertyService.createNew(propertyForm);
      await this.propertyService.save(property);

      this.onAfterSaveData();
  }

  public onAfterSaveData() {
    this.modalController.dismiss();
  }

  public onCancelForm() {
    this.modalController.dismiss();
  }
}
