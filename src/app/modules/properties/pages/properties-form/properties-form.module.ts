import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PropertiesFormPageRoutingModule } from './properties-form-routing.module';

import { PropertiesFormPage } from './properties-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PropertiesFormPageRoutingModule
  ],
  declarations: [PropertiesFormPage]
})
export class PropertiesFormPageModule {}
