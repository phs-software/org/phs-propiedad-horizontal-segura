import { Injectable } from '@angular/core';
import { Query } from 'parse';

import { Property, ParseProperty } from '../models/property';
import { PropertyData } from '../interfaces/property-data';

@Injectable({
  providedIn: 'root'
})
export class PropertiesService {
  public properties: Property[];

  constructor() { }

  /**
   * Loads the property data and a returns the model.
   */
  public async getProperty(id): Promise<Property> {
    const parseProperty = await this.loadParseProperty(id);
    return new Property(parseProperty);
  }

  /**
   * Creates a new property.
   */
  public createNew(property: PropertyData): Property {
    return new Property(property);
  }

  /**
   * Update the property data
   */
  public async save(property: Property) {
    let parseProperty: ParseProperty;

    if (property.id) {
    parseProperty = await this.loadParseProperty(property.id);
    } else {
    parseProperty = new ParseProperty();
    }

    parseProperty.set('address', property.address);

    await parseProperty.save();
    property.updateData(parseProperty);

    this.properties.push(property);

  }

  public loadProperties() {
    if (!this.properties) {
      this.properties = [];
    }
  }

  public async remove(property: Property) {
    const parseProperty = await this.loadParseProperty(property.id);
    await parseProperty.destroy();
  }

  /**
   * Loads the property from the DB using parse.
   */
  private async loadParseProperty(id): Promise<ParseProperty> {
    const query = new Query(ParseProperty);
    const parseProperty = await query.get(id);

    return parseProperty;
  }
}
