import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

import { Property } from '../../models/property';
import { PropertyData } from '../../interfaces/property-data';

@Component({
  selector: 'app-properties-list',
  templateUrl: './properties-list.component.html',
  styleUrls: ['./properties-list.component.scss'],
})
export class PropertiesListComponent implements OnInit {
  @Input()
  public properties: Property[];
  @Output()
  public editProperty: EventEmitter<PropertyData> = new EventEmitter();
  @Output()
  public removeProperty: EventEmitter<PropertyData> = new EventEmitter();

  public constructor() { }

  public ngOnInit() { }

  public edit(property: PropertyData) {
    this.editProperty.emit(property);
  }
  public remove(property: PropertyData) {
    this.removeProperty.emit(property);
  }
}
