import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { PropertyData } from '../../interfaces/property-data';

@Component({
  selector: 'app-property-form',
  templateUrl: './property-form.component.html',
  styleUrls: ['./property-form.component.scss'],
})
export class PropertyFormComponent implements OnInit {
  @Input()
  public propertyData: PropertyData;

  @Output()
  public submitForm: EventEmitter<{address: string}> = new EventEmitter();
  @Output()
  public cancelForm: EventEmitter<void> = new EventEmitter();

  private propertyForm: FormGroup;

  public constructor(
    private formBuilder: FormBuilder
  ) {
    this.propertyForm = this.formBuilder.group({
        address: ['', Validators.required],
    });
  }

  public ngOnInit() {
    if (this.propertyData) {
      this.propertyForm.setValue({
        address: this.propertyData.address,
      });
    }
  }

  public onSubmit(): void {
    const value = this.propertyForm.value;

    if (!this.propertyForm.valid) {
      // TODO: Define an error handling method.
      return;
    }

    if (this.propertyData) {
      value.id = this.propertyData.id;
    }

    this.submitForm.emit(value);
  }

  public onCancel(): void {
    this.cancelForm.emit();
  }
}
