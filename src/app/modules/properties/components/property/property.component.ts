import { Component, OnInit, Input } from '@angular/core';

import { Property } from '../../models/property';

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss'],
})
export class PropertyComponent implements OnInit {
  @Input()
  public property: Property;

  public constructor() {
  }

  public ngOnInit() {
  }
}
