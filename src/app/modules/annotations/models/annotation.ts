import { AnnotationData } from '../interfaces/annotation-data';
import { PersonData } from 'src/modules/residents/interfaces/person-data';
import { PropertyData } from 'src/modules/properties/interfaces/property-data';
import { FrontEndModel } from '@core/models/front-end-model';

export class Annotation extends FrontEndModel implements AnnotationData {
    public person: PersonData;
    public property: PropertyData;
    public title: string;
    public description: string;
    public dateStart: Date;
    public dateFinish: Date;
    public state: boolean; // 0 = available ;  1 = close

    public constructor(entity: AnnotationData) {
        super(entity);
        this.person = entity.person;
        this.property = entity.property;
        this.title = entity.title;
        this.description = entity.description;
        this.dateStart = entity.dateStart;
        this.dateFinish = entity.dateFinish;
        this.state = entity.state;
    }
}
