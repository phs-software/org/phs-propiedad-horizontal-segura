import { PersonData } from 'src/modules/residents/interfaces/person-data';
import { PropertyData } from 'src/modules/properties/interfaces/property-data';
import { BackEndDataModel } from 'src/app/interfaces/back-end-data-model';

export interface AnnotationData extends BackEndDataModel {
    person: PersonData;
    property: PropertyData;
    title: string;
    description: string;
    dateStart: Date;
    dateFinish: Date;
    state: boolean; // 0 = available ;  1 = close
}
