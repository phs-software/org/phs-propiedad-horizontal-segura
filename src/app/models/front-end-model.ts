import { BackEndDataModel } from '../interfaces/back-end-data-model';

/**
 * A base front end model related to a DB registry.
 */
export class FrontEndModel implements BackEndDataModel {
    public readonly id: string;
    public readonly createdAt: Date;
    public readonly updatedAt: Date;
    public readonly exists: boolean = false;

    public constructor(entity: BackEndDataModel) {

        if (entity.objectId) {
            this.id = entity.objectId;
            this.exists = true;
        }

        if (entity.createdAt) {
            this.createdAt = new Date(entity.createdAt);
        }

        if (entity.updatedAt) {
            this.updatedAt = new Date(entity.updatedAt);
        }
    }
}
